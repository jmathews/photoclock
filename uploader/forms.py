from django import forms
from models import Photo


class PhotoUploadForm(forms.ModelForm):
    valid_time_formats = ['%H:%M', '%I:%M%p', '%I:%M %p']

    photo_file = forms.ImageField(label='Submit a photo')
    photo_time = forms.TimeField(label='What time is visible in the photo?', input_formats=valid_time_formats)
    submitter_email = forms.EmailField(label='What is your email address?', required=False)
    submitter_twitter = forms.CharField(label='What is your Twitter username?', required=False)

    def __init__(self, *args, **kwargs):
        super(PhotoUploadForm, self).__init__(*args, **kwargs)
        self.fields['photo_file'].widget.attrs = {'class': 'btn-default btn-lg filepickers'}
        self.fields['photo_time'].widget.attrs = {'class': 'btn-default btn-lg timesliders'}
        self.fields['submitter_email'].widget.attrs = {'class': 'btn-default btn-lg emails'}
        self.fields['submitter_twitter'].widget.attrs = {'class': 'btn-default btn-lg twitter-user'}

    class Meta:
        model = Photo
        fields = ['photo_file', 'photo_time', 'submitter_email', 'submitter_twitter']

    def get_original_filename(self):
        if len(self.files) > 0:
            if self.prefix:
                files_index = self.prefix + '-photo_file'
            else:
                files_index = 'photo_file'
            return self.files[files_index].name
        else:
            return None


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField(label='Your Email Address')
    cc_myself = forms.BooleanField(required=False, initial=True)

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['subject'].widget.attrs = {'class':'btn-default btn-lg'}
        self.fields['message'].widget.attrs = {'class':'btn-default btn-lg'}
        self.fields['sender'].widget.attrs = {'class':'btn-default btn-lg'}
        self.fields['cc_myself'].widget.attrs = {'class':'btn-default btn-lg'}
