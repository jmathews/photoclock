# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0009_photo_submitter_ip'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='photo_time',
            field=models.TimeField(default='00:00'),
            preserve_default=False,
        ),
    ]
