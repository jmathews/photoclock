# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0017_auto_20141230_0709'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='is_posted_to_twitter',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
