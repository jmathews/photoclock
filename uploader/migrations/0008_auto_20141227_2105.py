# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0007_photo_submitter'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photo',
            name='submitter',
        ),
        migrations.AddField(
            model_name='photo',
            name='submitter_email',
            field=models.EmailField(default='photographer@example.org', max_length=254),
            preserve_default=False,
        ),
    ]
