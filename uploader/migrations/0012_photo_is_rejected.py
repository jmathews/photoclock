# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0011_photo_is_approved'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='is_rejected',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
