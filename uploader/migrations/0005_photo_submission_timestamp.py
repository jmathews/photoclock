# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0004_auto_20141227_0659'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='submission_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 20, 45, 39, 130437)),
            preserve_default=True,
        ),
    ]
