# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0014_photo_submitter_twitter'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='submitter_twitter',
            field=models.CharField(max_length=15, null=True),
            preserve_default=True,
        ),
    ]
