# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0008_auto_20141227_2105'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='submitter_IP',
            field=models.GenericIPAddressField(default='127.0.0.1'),
            preserve_default=False,
        ),
    ]
