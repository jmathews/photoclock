# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uploader.models


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0019_auto_20150125_2320'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='approved_photo_file',
            field=models.ImageField(null=True, upload_to=uploader.models.approved_file_path, blank=True),
            preserve_default=True,
        ),
    ]
