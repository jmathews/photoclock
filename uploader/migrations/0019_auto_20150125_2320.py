# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uploader.models


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0018_photo_is_posted_to_twitter'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='original_photo_file_name',
            field=models.CharField(max_length=1024, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='photo',
            name='photo_file',
            field=models.ImageField(null=True, upload_to=uploader.models.get_file_path),
            preserve_default=True,
        ),
    ]
