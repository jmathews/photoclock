# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0012_photo_is_rejected'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='is_copied_to_approved_folder',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
