from django.shortcuts import render
from django.core.mail import send_mail
from django.contrib import messages
from models import Config, Photo
from forms import PhotoUploadForm, ContactForm
from photo_upload_form_processor import PhotoUploadFormProcessor
from photoclock.settings import CONTACT_FORM_RECIPIENT
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from easy_thumbnails.files import get_thumbnailer
from django.conf import settings
from django.core.mail import EmailMessage


def call_for_submissions(request, template='uploader/call_for_submissions.html'):
    if request.method == 'POST':
        form = PhotoUploadForm(data=request.POST, files=request.FILES)
        photo_upload_form_processor = PhotoUploadFormProcessor(request, form)
        photo_upload_form_processor.process()

    context = {}
    context['photo_upload_form'] = PhotoUploadForm()
    context['contact_form'] = ContactForm()
    context['min_width'] = Config.get_config_int('min_width')
    context['min_height'] = Config.get_config_int('min_height')
    context['max_file_size_MB'] = Config.get_config_int('max_file_size_MB')
    return render(request, template, context)


def contact(request, template='uploader/call_for_submissions.html'):
    if request.method == 'POST':
        form = ContactForm(data=request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = settings.DEFAULT_FROM_EMAIL
            reply_to = form.cleaned_data['sender']
            cc_myself = form.cleaned_data['cc_myself']

            recipients = [Config.get_config_string('contact_form_email_recipient', CONTACT_FORM_RECIPIENT)]
            if cc_myself:
                recipients.append(form.cleaned_data['sender'])

            try:
                email = EmailMessage(subject, message, sender,
                                     recipients,
                                     headers={'Reply-To': reply_to})
                email.send()

                messages.success(request, 'Mail was successfully sent.')
            except Exception as e:
                messages.error(request, 'Mail was not successfully sent.')
                raise e

        else:
            messages.error(request, 'Mail form was not filled in properly.')

    return HttpResponseRedirect(redirect_to=reverse(viewname='call_for_submissions'))


def thumbnail_url(image_path, resolution=(200, 200)):
    thumbnailer = get_thumbnailer(image_path)
    thumbnail_options = {'crop': False, 'size': resolution, 'detail': True, 'upscale':False }
    thumbnail = thumbnailer.get_thumbnail(thumbnail_options)
    return u'{media_url}{thumbnail_path}'.format(media_url=settings.MEDIA_URL, thumbnail_path=thumbnail)


def photo_show(request, photo_id, template='uploader/show.html'):
    try:
        photo = Photo.objects.get(id=photo_id)
        context = {}
        context['photo'] = photo
        context['thumbnail_url'] = thumbnail_url(photo.photo_file.name, resolution=(1024,0))
        return render(request, template, context)
    except Photo.DoesNotExist:
        return HttpResponseRedirect(redirect_to=reverse(viewname='call_for_submissions'))
